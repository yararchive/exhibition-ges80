/* humanstxt.org */

/* TEAM */
  project director: Andrey Zvorygin
  e-mail: umount.dev.brain@ya.ru
  vk: http://vk.com/umount.dev.brain
  location: Yaroslavl, Russia

/* SITE */                          
  Last update: 2015/04/27 
  Standards: HTML5, CSS3
  Components: jQuery 2.1.3, Modernizr 2.8.3, Normalize.css 3.0.3, Backbone.JS 1.1.2, Underscore.JS 1.8.3, JSON2.js
  Software: Sublime Text 3, Git