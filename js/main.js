$(document).ready(function() {

  $(window).load(function() {
    $("#content-loader").delay(1000).fadeOut(1000);

    $("#parts-block .part, #top-menu .part").click(function(e) {
      switch($(this).attr("class")) {
        case "part p0":
          $("section.sect").fadeOut(500);
          $("#top-menu").fadeOut(500);
          $("#part0").fadeIn(500);
          console.log("Переход к части 0");
          break;
        case "part p1":
          $("section.sect").fadeOut(500);
          $("#top-menu").fadeIn(500);
          $("#part1").fadeIn(500);
          console.log("Переход к части 1");
          break;
        case "part p2":
          $("section.sect").fadeOut(500);
          $("#top-menu").fadeIn(500);
          $("#part2").fadeIn(500);
          console.log("Переход к части 2");
          break;
        case "part p3":
          $("section.sect").fadeOut(500);
          $("#top-menu").fadeIn(500);
          $("#part3").fadeIn(500);
          console.log("Переход к части 3");
          break;
        case "part p4":
          $("section.sect").fadeOut(500);
          $("#top-menu").fadeIn(500);
          $("#part4").fadeIn(500);
          console.log("Переход к части 4");
          break;
        case "part p5":
          $("section.sect").fadeOut(500);
          $("#top-menu").fadeIn(500);
          $("#part5").fadeIn(500);
          console.log("Переход к части 5");
          break;
        default:
          alert("Ошибка при выборе раздела выставки");
      }
    });
  });

});